import asyncio
from datetime import datetime

import requests
import json
from aiohttp import ClientSession

from pathlib import Path

import utils
from package import Package

PYPI_JSON_API = "https://pypi.org/pypi/{}/json"
PYPI_SIMPLE_URL = "https://pypi.org/simple/{}"

TASK_LIMIT = 50


class ProjectNotFound(Exception):
    pass


class Project:
    def __init__(self, name: str, download_path: str = "."):
        self.name = name
        if not self._is_project_exists():
            raise ProjectNotFound(f"Project: {self.name} not found!")
        self.download_path = download_path
        self.json = self._get_json()
        self.requires_dist = self.json["info"]["requires_dist"]
        self.packages = self._get_all_packages()

    def download_project(self, download_path: str = None, from_date: datetime = None, to_date: datetime = None):
        if not download_path:
            download_path = self.download_path
        download_path = Path(download_path)
        utils.create_directories_path_if_not_exists(download_path)
        asyncio.run(self.download_packages(download_path, from_date, to_date))
        self.download_simple(download_path)

    def download_simple(self, download_path: str = None):
        if not download_path:
            download_path = self.download_path
        download_path = Path(download_path)
        download_path = download_path.joinpath(f'simple/{self.name}/index.html')
        utils.create_directories_path_if_not_exists(download_path)
        parsed_simple_content = self._get_parsed_simple_content()
        with open(download_path, "wb") as simple_file:
            simple_file.write(parsed_simple_content)

    async def download_packages(self, download_path: str = None, from_date: datetime = None, to_date: datetime = None):
        if not download_path:
            download_path = self.download_path
        tasks = []
        async with ClientSession() as session:
            for package in self.packages:
                if (from_date is None or package.upload_date >= from_date) and \
                        (to_date is None or package.upload_date < to_date):
                    task = package.download(session, download_path)
                    tasks.append(task)
                    if len(tasks) > TASK_LIMIT:
                        await asyncio.gather(*tasks)
                        tasks = []
            await asyncio.gather(*tasks)

    def _is_project_exists(self):
        response = requests.get(PYPI_JSON_API.format(self.name))
        return response.status_code == requests.status_codes.codes.OK

    def _get_parsed_simple_content(self):
        simple_response = requests.get(PYPI_SIMPLE_URL.format(self.name))
        return simple_response.content.replace("https://files.pythonhosted.org/packages/".encode(),
                                               "../../packages/".encode())

    def _get_all_packages(self):
        packages = []
        for version_info in self.json["releases"].values():
            for package_info in version_info:
                new_package = Package(package_info["filename"], package_info["url"],
                                      package_info["upload_time"], package_info["md5_digest"], package_info["size"])
                packages.append(new_package)
        return packages

    def _get_json(self):
        response = requests.get(PYPI_JSON_API.format(self.name))
        return json.loads(response.content)
