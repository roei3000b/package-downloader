from datetime import datetime
from pathlib import Path

from aiohttp import ClientSession

import utils
import re


class Package:
    def __init__(self, name: str, url: str, upload_time: str, md5: str, size: int):
        self.name = name
        self.download_url = url
        self.upload_time = upload_time
        self.upload_date = self._upload_time_to_upload_date()
        self.md5 = md5
        self.size = size

    async def download(self, session: ClientSession, download_path: str = "./"):
        local_file_location = Path(re.findall("packages/.*", self.download_url)[0])
        download_path = Path(download_path).joinpath(local_file_location)
        utils.create_directories_path_if_not_exists(download_path)
        await utils.download_file(self.download_url, download_path, session)

    def _upload_time_to_upload_date(self):
        upload_date = self.upload_time.partition("T")[0]
        return utils.string_date_to_datetime(upload_date)
