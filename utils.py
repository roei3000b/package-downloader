from datetime import datetime
from pathlib import Path
import errno
import os

import aiofiles
import requests
from aiohttp import ClientSession


def create_directories_path_if_not_exists(file_path: str):
    file_path = Path(file_path)
    if not file_path.parent.exists():
        try:
            os.makedirs(file_path.parent)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
    return file_path


def string_date_to_datetime(date: str) -> datetime:
    return datetime.strptime(date, "%Y-%m-%d")


async def download_file(url: str, target: Path, session: ClientSession, chunk_size: int = 4*1024, **kwargs):
    async with session.get(url, **kwargs) as response:
        assert response.status == requests.status_codes.codes.OK
        async with aiofiles.open(target, "wb") as f:
            while True:
                chunk = await response.content.read(chunk_size)
                if not chunk:
                    break
                await f.write(chunk)
    return target



